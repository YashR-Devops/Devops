

resource "aws_vpc" "terraform-vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "terraform-vpc"
  }
}

resource "aws_subnet" "public-subnet" {
  vpc_id     = aws_vpc.terraform-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    Name = "public-subnet"
  }
}

resource "aws_ebs_volume" "test1" {
  availability_zone = "ap-south-1a"
  size              = 1

  tags = {
    Name = "test1"
  }
}

resource "aws_instance" "web" {
  ami           = "ami-01216e7612243e0ef"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.public-subnet.id
  tags = {
    Name = "HelloWorld"
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.test1.id
  instance_id = aws_instance.web.id
}



resource "aws_security_group_rule" "example" {
  type              = "ingress"
  from_port         = 0
  to_port           = 8080
  protocol          = "tcp"
  cidr_blocks       = [aws_vpc.terraform-vpc.cidr_block]
  security_group_id = aws_vpc.terraform-vpc.default_security_group_id
} 